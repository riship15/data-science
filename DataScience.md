![image](images/cogclass.png "Cognitive Class")

<a href="https://www.bigdatauniversity.com"><img src="https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/Images/CCLog.png" width=300, align="center"></a>

<h1 align=center><font size = 5>Data Analytics </font></h1>

<h1>Introduction</h1>


<p>
In this part we will get to know the approach of Data Acquisition.Through this Lab, we will load the data into Jupyter Notebook. 
</p>

<h2> Content</h2>

<div class="alert alert-block alert-info" style="margin-top: 20px">
<ol>
    <li><a href="#data_acquisition">Data Acquisition</a>
    <li><a href="#basic_insight"> Insight of Dataset</a></li>
</ol>


</div>
<hr>


<h1 id="data_acquisition">Data Acquisition</h1>
<p>

Here , we will get to know how to load a dataset into our Jupyter Notebook.<br>


The Pandas Library is a useful tool that enables us to read various datasets into a data frame; our Jupyter notebook platforms have a built-in <b>Pandas Library</b> so that all we need to do is import Pandas without installing.
</p>

```python

import pandas as pd
```


```python
other_path = "https://archive.ics.uci.edu/ml/machine-learning-databases/autos/imports-85.data"
df = pd.read_csv(other_path)
df
```
<h2>Read </h2>
<p>
We use <code>pandas.read_csv()</code> function to read the csv file.  The file path can be either an URL or your local file address.<br>

</p>

This dataset was hosted on IBM Cloud object click <a href="https://cocl.us/DA101EN_object_storage">HERE</a> for free storage.


```python# Import pandas library
import pandas as pd

# Read the online file by the URL provides above, and assign it to variable "df"
other_path = "https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DA0101EN/auto.csv"

df = pd.read_csv(other_path, header=None)
df

```
 Contrary to <code>dataframe.head(n)</code>, <code>dataframe.tail(n)</code> will show you the bottom n rows of the dataframe.
```python
# show the first 5 rows using dataframe.head() method
print("The first 5 rows of the dataframe") 
print(df.head(5))
#size
print("size=",df.size)
#dimensions
print("Dimnesions=",df.ndim)
#shape
print("Shape=",df.shape)
df.head()

```
<div class="alert alert-danger alertdanger" style="margin-top: 20px">
<h1> Problem: </h1>
<b>check the bottom 10 rows of data frame "df".</b>
</div>

```python
# Write your code below and press Shift+Enter to execute 
print("The last 10 rows of the dataframe\n")
df.tail(10)
```

Double-click <b>here</b> for the solution.
​
<!-- The answer is below:
​
print("The last 10 rows of the dataframe\n")
df.tail(10)
​
--->
<h3>Add Header</h3>
<p>
Take a look at our dataset.
</p>
<p>
To describe the data we can introduce a header, this information is available at:  <a href="https://archive.ics.uci.edu/ml/datasets/Automobile" target="_blank">https://archive.ics.uci.edu/ml/datasets/Automobile</a>
</p>
<p>
Thus, we have to add headers manually.
</p>
<p>
We create a list "headers" that include all column names in order.
Then, we use <code>dataframe.columns = headers</code> to replace the headers by the list we created.
</p>

```python
# create headers list
headers = ["symboling","normalized-losses","make","fuel-type","aspiration", "num-of-doors","body-style",
         "drive-wheels","engine-location","wheel-base", "length","width","height","curb-weight","engine-type",
         "num-of-cylinders", "engine-size","fuel-system","bore","stroke","compression-ratio","horsepower",
         "peak-rpm","city-mpg","highway-mpg","price"]
print("headers\n", headers)
```
```python

import numpy as np

df.columns = headers
df['price'].replace('?',np.nan,inplace=True)
avg=df['price'].astype("float").mean(axis=0)
df['price'].replace(np.nan,avg,inplace=True) 
df['price']=df['price'].astype('float')
df_test=df[['body-style','drive-wheels', 'price']]

df_test.head()
df_grp=df_test.groupby(['drive-wheels','body-style'],as_index=False).mean()
#df_gptest.groupby(['drive-wheels','body-style'],as_index=False).mean()
df.dtypes
df_grp
#df1=pd.DataFrame()
```
```python
df.dropna(subset=["price"], axis=0)
```
We have read the  dataset and add the correct headers into the data frame.
 <div class="alert alert-danger alertdanger" style="margin-top: 20px">
<h1> Problem </h1>
<b>Find columns of the dataframe</b>
</div>

Double-click <b>here</b> for the solution.

<!-- The answer is below:

print(df.columns)
-->

<h2>Save Dataset</h2>

<p>
    For example, if you would save the dataframe <b>df</b> as <b>automobile.csv</b> to your local machine, you may use the syntax below:
</p>

```python

#df.to_csv("automobile.csv", index=False)
path="C:\\Users\\Dell\\Documents\\two.csv"
df.to_csv(path)
```
We can  read and save other file formats, we can use similar functions to **`pd.read_csv()`** and **`df.to_csv()`** for other data formats, the functions are listed :

<h2>Read/Save Other Data Formats</h2>



| Data   | Read           | Save             |
| ------------- |:--------------:| ----------------:|
| csv           | `pd.read_csv()`  |`df.to_csv()`     |
| json          | `pd.read_json()` |`df.to_json()`    |




